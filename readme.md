# WFont

## Configuration
```
extension:
    wfont: Slts\WFont\DI\WFontExtension

wfont:
    fonts: 'Work Sans' # string or string[]
    className: wf-active # default
    cookieName: wfont # default
    loadTimeout: 5000 # default
    additionalScripts: # string or string[] with src or array of arrays with script element attributes, default []
        - https://cdnjs.cloudflare.com/ajax/libs/fontfaceobserver/2.0.13/fontfaceobserver.standalone.js
        - [
            'src': 'https://cdnjs.cloudflare.com/ajax/libs/fontfaceobserver/2.0.13/fontfaceobserver.standalone.js'
            'data-attr': 'data-val'
        ]
    autowired: true # default
    latteRegister: true # default, if register latte macros and add intance as provider into Engine
```
