<?php

namespace Slts\WFont\DI;

use Nette\DI\CompilerExtension;
use Nette\Http\IRequest;
use Slts\WFont\WFont;

class WFontExtension extends CompilerExtension
{
    private $defaults = [
        'fonts' => [],
        'className' => 'wf-active',
        'cookieName' => 'wfont',
        'additionalScripts' => [],
        'loadTimeout' => 5000,
        'autowired' => true,
        'latteRegister' => true,
    ];

    public function loadConfiguration()
    {
        $config = $this->validateConfig($this->defaults);
        $builder = $this->getContainerBuilder();
        $builder->addDefinition($this->prefix('wfont'))
            ->setType(WFont::class)
            ->setArguments([
                (array)$config['fonts'],
                $config['className'],
                $config['cookieName'],
                (array)$config['additionalScripts'],
                $config['loadTimeout'],
                $builder->getDefinitionByType(IRequest::class, false)
            ])
            ->setAutowired($config['autowired'])
        ;
    }

    public function beforeCompile()
    {
        $builder = $this->getContainerBuilder();
        $config = $this->validateConfig($this->defaults);

        if ($config['latteRegister']) {
            $def = $builder->getDefinition($this->prefix('wfont'));
            $latte = $builder->getDefinition('latte.latteFactory');
            $latte->addSetup('?->addProvider(?, ?)', ['@self', '_wfont', $def]);
            $latte->addSetup('?->onCompile[] = function($engine) { Slts\WFont\Latte\Macros\WFontMacros::install($engine->getCompiler()); }', ['@self']);
        }
    }
}
