<?php
namespace Slts\WFont\Latte\Macros;

use Latte\Compiler;
use Latte\Macros\MacroSet;

class WFontMacros extends MacroSet
{
    public static function install(Compiler $compiler)
    {
        /** @var WFontMacros $me */
        $me = new static($compiler);

        $me->addMacro('wFont', 'echo $this->global->_wfont->render();');
        $me->addMacro('wFontClass', 'echo $this->global->_wfont->className();');

        return $me;
    }
}
