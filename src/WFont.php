<?php

namespace Slts\WFont;

use Nette\Http\IRequest;
use Nette\Utils\Html;

class WFont
{
    private $additionalScripts;
    private $className;
    private $cookieName;
    private $fontNames;
    private $loadTimeout;
    private $isLoaded;

    public function __construct(
        array $fontNames,
        string $className,
        string $cookieName,
        array $additionalScripts,
        int $loadTimeout,
        IRequest $request
    ) {
        $this->fontNames = $fontNames;
        $this->className = $className;
        $this->cookieName = $cookieName;
        $this->additionalScripts = $additionalScripts;
        $this->loadTimeout = $loadTimeout;
        $this->isLoaded = (bool)$request->getCookie($this->cookieName, null);
    }

    public function render(): ?Html
    {
        if ($this->isLoaded || !$this->fontNames) {
            return null;
        }

        $fonts = implode(', ', array_map(function ($fontName) {
            return "(new FontFaceObserver('{$fontName}')).load(null, {$this->loadTimeout})";
        }, $this->fontNames));
        $js = <<<JS
Promise.all([{$fonts}])
.then(function () {
    document.cookie = '{$this->cookieName}=1; expires='+(new Date(new Date().getTime() + 86400000)).toGMTString()+'; path=/'
}, function () {
    console.log('Font loading failed');
})
.finally(function() {
   document.documentElement.className += " {$this->className}";
});
JS;
        $wrapper = Html::el();
        if ($this->additionalScripts) {
            foreach ($this->additionalScripts as $scriptDefinition) {
                $attrs = is_string($scriptDefinition) ? ['src' => $scriptDefinition] : $scriptDefinition;
                $wrapper->addHtml(Html::el('script', $attrs));
            }
        }
        $wrapper->addHtml(Html::el('script')->setHtml($js));
        return $wrapper;
    }

    public function className(): string
    {
        return $this->isLoaded ? $this->className : '';
    }
}
